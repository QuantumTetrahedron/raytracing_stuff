
#ifndef RAYTRACING_STUFF_BVH_H
#define RAYTRACING_STUFF_BVH_H

#include "Hitable.h"
#include "Randoms.h"
#include <algorithm>
#include <memory>

class BVH : public Hitable{
public:

    BVH() = default;

    BVH(const std::vector<std::shared_ptr<Hitable>>& l){
        int axis = int(3 * randomFloat());

        std::sort(l.begin(), l.end(), [axis](const std::shared_ptr<Hitable>& h1, const std::shared_ptr<Hitable>& h2){
           AABB box_left, box_right;
           if(h1->boundingBox(box_left) && h2->boundingBox(box_right)){
               return box_left.min[axis] < box_right.min[axis];
           } else return false;
        });

        if(l.size() == 1){
            left = right = l[0];
        }
        else if(l.size() == 2){
            left = l[0];
            right = l[1];
        }
        else {
            std::size_t const half_size = l.size() / 2;
            left = std::make_shared<BVH>(std::vector<std::shared_ptr<Hitable>>(l.begin(), l.begin() + half_size));
            right = std::make_shared<BVH>(std::vector<std::shared_ptr<Hitable>>(l.begin() + half_size, l.end()));
        }

        AABB box_left, box_right;
        if(left->boundingBox(box_left) && right->boundingBox(box_right)){
            box = box_left.add(box_right);
        }
    }

    bool hit(const Ray &r, float t_min, float t_max, HitRecord &rec) const override {
        if(box.hit(r, t_min, t_max)){
            HitRecord left_rec, right_rec;
            bool hit_left = left->hit(r, t_min, t_max, left_rec);
            bool hit_right = right->hit(r, t_min, t_max, right_rec);
            if(hit_left && hit_right){
                rec = left_rec.t < right_rec.t ? left_rec : right_rec;
                return true;
            } else if(hit_left){
                rec = left_rec;
                return true;
            } else if(hit_right){
                rec = right_rec;
                return true;
            } else return false;
        }
        else return false;
    }

    bool boundingBox(AABB &b) const override {
        b = box;
        return true;
    }

    std::shared_ptr<Hitable> left;
    std::shared_ptr<Hitable> right;
    AABB box;
};

#endif //RAYTRACING_STUFF_BVH_H
