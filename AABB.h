
#ifndef RAYTRACING_STUFF_AABB_H
#define RAYTRACING_STUFF_AABB_H

#include <glm/vec3.hpp>
#include "Ray.h"

class AABB {
public:
    AABB() = default;
    AABB(const glm::vec3& a, const glm::vec3& b)
    : min(a), max(b){}

    [[nodiscard]] bool hit(const Ray& r, float tmin, float tmax) const {
        for(int a = 0; a<3; a++){
            float invD = 1.0f / r.direction[a];
            float t0 = (min[a]-r.origin[a]) * invD;
            float t1 = (max[a]-r.origin[a]) * invD;
            if(invD < 0.0f)
                std::swap(t0,t1);
            tmin = t0 > tmin ? t0 : tmin;
            tmax = t1 < tmax ? t1 : tmax;
            if(tmax < tmin)
                return false;
        }
        return true;
    }

    AABB add(const AABB& other){
        glm::vec3 small(fmin(min.x, other.min.x), fmin(min.y, other.min.y), fmin(min.z, other.min.z));
        glm::vec3 big(fmax(max.x, other.max.x), fmax(max.y, other.max.y), fmax(max.z, other.max.z));
        return AABB(small, big);
    }

    glm::vec3 min, max;
};

#endif //RAYTRACING_STUFF_AABB_H
