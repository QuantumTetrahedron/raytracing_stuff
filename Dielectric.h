
#ifndef RAYTRACING_STUFF_DIELECTRIC_H
#define RAYTRACING_STUFF_DIELECTRIC_H

#include "Material.h"

class Dielectric : public Material{
public:
    Dielectric(float ri) : ref_idx(ri){}

    bool scatter(const Ray &r_in, const HitRecord &rec, glm::vec3 &attenuation, Ray &scattered) const override {
        glm::vec3 outward_normal;
        glm::vec3 reflected = glm::reflect(r_in.direction, rec.normal);
        float ni_over_nt;
        attenuation = glm::vec3(1.0f,1.0f,1.0f);
        glm::vec3 refracted;

        float reflect_prob;
        float cosine;

        if(glm::dot(r_in.direction, rec.normal) > 0){
            outward_normal = -rec.normal;
            ni_over_nt = ref_idx;
            cosine = ref_idx * glm::dot(r_in.direction, rec.normal) / glm::length(r_in.direction);
        } else {
            outward_normal = rec.normal;
            ni_over_nt = 1.0f / ref_idx;
            cosine = -glm::dot(r_in.direction, rec.normal) / glm::length(r_in.direction);
        }

        refracted = glm::refract(glm::normalize(r_in.direction), outward_normal, ni_over_nt);
        if(glm::length(refracted) > 0.0){
            reflect_prob = schlick(cosine, ref_idx);
        } else {
            reflect_prob = 1.0;
        }

        if(randomFloat() < reflect_prob){
            scattered = Ray(rec.p, reflected);
        } else {
            scattered = Ray(rec.p, refracted);
        }
        return true;
    }

    static float schlick(float cosine, float ref_id){
        float r0 = (1-ref_id) / (1+ref_id);
        r0 *= r0;
        return r0 + (1.0f-r0)*glm::pow(1.0f-cosine, 5.0f);
    }

    float ref_idx;
};

#endif //RAYTRACING_STUFF_DIELECTRIC_H
