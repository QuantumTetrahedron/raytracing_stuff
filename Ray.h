
#ifndef RAYTRACING_STUFF_RAY_H
#define RAYTRACING_STUFF_RAY_H

#include <glm/glm.hpp>

class Ray{
public:
    Ray() = default;

    Ray(const glm::vec3& a, const glm::vec3& b)
    : origin(a), direction(b){}

    [[nodiscard]] glm::vec3 pointAtParameter(float t) const {
        return origin + t * direction;
    }

    glm::vec3 origin, direction;
};

#endif //RAYTRACING_STUFF_RAY_H
