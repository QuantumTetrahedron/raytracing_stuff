
#ifndef RAYTRACING_STUFF_TEXTURE_H
#define RAYTRACING_STUFF_TEXTURE_H

#include <glm/vec3.hpp>

class Texture{
public:
    [[nodiscard]] virtual glm::vec3 value(float u, float v, const glm::vec3& p) const = 0;
};

class ConstantTexture : public Texture {
public:
    explicit ConstantTexture(glm::vec3 c) : color(c) {}

    [[nodiscard]] glm::vec3 value(float u, float v, const glm::vec3 &p) const override {
        return color;
    }

    glm::vec3 color;
};

class CheckerTexture : public Texture{
public:
    CheckerTexture(std::shared_ptr<Texture> t0, std::shared_ptr<Texture> t1)
    : even(std::move(t0)), odd(std::move(t1)){}

    [[nodiscard]] glm::vec3 value(float u, float v, const glm::vec3 &p) const override {
        float sines = glm::sin(10*p.x) * glm::sin(10*p.y) * glm::sin(10*p.z);
        if(sines < 0){
            return odd->value(u,v,p);
        } else {
            return even->value(u,v,p);
        }
    }

    std::shared_ptr<Texture> even, odd;
};

#endif //RAYTRACING_STUFF_TEXTURE_H
