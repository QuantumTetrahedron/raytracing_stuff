
#ifndef RAYTRACING_STUFF_LAMBERTIAN_H
#define RAYTRACING_STUFF_LAMBERTIAN_H

#include "Material.h"
#include "Randoms.h"
#include "Texture.h"

class Lambertian : public Material{
public:
    explicit Lambertian(std::shared_ptr<Texture> a) : albedo(std::move(a)){}

    bool scatter(const Ray &r_in, const HitRecord &rec, glm::vec3& attenuation, Ray &scattered) const override {
        glm::vec3 target = rec.p + rec.normal + randomInUnitSphere();
        scattered = Ray(rec.p, target - rec.p);
        attenuation = albedo->value(rec.u,rec.v,rec.p);
        return true;
    }

    std::shared_ptr<Texture> albedo;
};

#endif //RAYTRACING_STUFF_LAMBERTIAN_H
