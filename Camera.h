
#ifndef RAYTRACING_STUFF_CAMERA_H
#define RAYTRACING_STUFF_CAMERA_H

#include "Ray.h"
#include "Randoms.h"

class Camera{
public:
    Camera(glm::vec3 position, glm::vec3 at, glm::vec3 up, float vfov, float aspect, float aperture, float focus_distance){
        lens_radius = aperture / 2.0f;
        float theta = glm::radians(vfov);
        float half_height = glm::tan(theta / 2.0f);
        float half_width = aspect * half_height;
        origin = position;
        w = glm::normalize(position - at);
        u = glm::normalize(glm::cross(up, w));
        v = glm::cross(w, u);
        lower_left_corner = origin - (half_width * u + half_height * v + w) * focus_distance;
        horizontal = 2.0f * half_width * focus_distance * u;
        vertical = 2.0f * half_height * focus_distance * v;
    }

    Ray getRay(float s, float t){
        glm::vec3 rd = lens_radius * randomInUnitDisk();
        glm::vec3 offset = u * rd.x + v*rd.y;
        glm::vec3 o = origin + offset;
        return Ray(o, lower_left_corner + s * horizontal + t * vertical - o);
    }

    glm::vec3 origin;
    glm::vec3 lower_left_corner;
    glm::vec3 horizontal;
    glm::vec3 vertical;
    glm::vec3 u,v,w;
    float lens_radius;
};

#endif //RAYTRACING_STUFF_CAMERA_H
