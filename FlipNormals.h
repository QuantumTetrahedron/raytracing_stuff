
#ifndef RAYTRACING_STUFF_FLIPNORMALS_H
#define RAYTRACING_STUFF_FLIPNORMALS_H

#include "Hitable.h"

class FlipNormals : public Hitable{
public:
    explicit FlipNormals(std::shared_ptr<Hitable> _h) : h(std::move(_h)){}

    bool hit(const Ray &r, float t_min, float t_max, HitRecord &rec) const override {
        if(h->hit(r, t_min, t_max, rec)){
            rec.normal = -rec.normal;
            return true;
        } else return false;
    }

    bool boundingBox(AABB &box) const override {
        return h->boundingBox(box);
    }

    std::shared_ptr<Hitable> h;
};

#endif //RAYTRACING_STUFF_FLIPNORMALS_H
