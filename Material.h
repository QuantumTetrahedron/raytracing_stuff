
#ifndef RAYTRACING_STUFF_MATERIAL_H
#define RAYTRACING_STUFF_MATERIAL_H

#include "Ray.h"
#include "Hitable.h"

class Material{
public:
    virtual bool scatter(const Ray& r_in, const HitRecord& rec, glm::vec3& attenuation, Ray& scattered) const = 0;
    [[nodiscard]] virtual glm::vec3 emitted(float u, float v, const glm::vec3& p) const { return glm::vec3(0.0f);}
};

#endif //RAYTRACING_STUFF_MATERIAL_H
