#include <iostream>
#include <glm/glm.hpp>
#include <memory>
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#include "Ray.h"
#include "Hitable.h"
#include "Sphere.h"
#include "HitableList.h"
#include "Camera.h"
#include "Randoms.h"
#include "Lambertian.h"
#include "Metal.h"
#include "Dielectric.h"
#include "Perlin.h"
#include "NoiseTexture.h"
#include "ImageTexture.h"
#include "Rects.h"
#include "DiffuseLight.h"
#include "FlipNormals.h"

std::unique_ptr<Hitable> randomScene(){
    std::vector<std::shared_ptr<Hitable>> list;
    std::shared_ptr<Texture> tex = std::make_shared<CheckerTexture>(
            std::make_shared<ConstantTexture>(glm::vec3(0.2, 0.3, 0.1)),
            std::make_shared<ConstantTexture>(glm::vec3(0.9)));
    list.push_back(std::make_shared<Sphere>(glm::vec3(0.0f, -1000.0f, 0.0f), 1000.0f, std::make_unique<Lambertian>(tex)));
    for(int a = -11; a < 11; a++){
        for(int b = -11; b < 11; b++){
            float choose_mat = randomFloat();
            glm::vec3 center(a+ 0.9 * randomFloat(), 0.2, b + 0.9 * randomFloat());
            if(glm::length(center - glm::vec3(4.0,0.2,0.0)) > 0.9){
                std::shared_ptr<Material> mat;
                if(choose_mat < 0.8){
                    tex = std::make_shared<ConstantTexture>(glm::vec3(randomFloat() * randomFloat(), randomFloat() *
                                                                                                     randomFloat(),
                                                                       randomFloat() *
                                                                                                                                     randomFloat()));
                    mat = std::make_shared<Lambertian>(tex);
                } else if(choose_mat < 0.95){
                    tex = std::make_shared<ConstantTexture>(glm::vec3(0.5f * (1.0f + randomFloat()), 0.5f * (1.0f +
                                                                                                             randomFloat()), 0.5f * (1.0f +
                                                                                                                                        randomFloat())));
                    mat = std::make_shared<Metal>(tex, 0.5f * randomFloat());
                } else {
                    mat = std::make_shared<Dielectric>(1.5f);
                }
                list.push_back(std::make_shared<Sphere>(center, 0.2, mat));
            }
        }
    }
    list.push_back(std::make_shared<Sphere>(glm::vec3(0, 1, 0), 1.0, std::make_unique<Dielectric>(1.5)));
    tex = std::make_shared<ConstantTexture>(glm::vec3(0.4, 0.2, 0.1));
    list.push_back(std::make_shared<Sphere>(glm::vec3(-4, 1, 0), 1.0, std::make_unique<Lambertian>(tex)));
    tex = std::make_shared<ConstantTexture>(glm::vec3(0.7, 0.6, 0.5));
    list.push_back(std::make_shared<Sphere>(glm::vec3(4, 1, 0), 1.0, std::make_unique<Metal>(tex, 0.0)));

    return std::make_unique<HitableList>(list);
}

std::unique_ptr<Hitable> twoPerlinSpheres(){
    std::shared_ptr<Texture> pertext = std::make_shared<NoiseTexture>(4.0f);
    std::vector<std::shared_ptr<Hitable>> list;

    int nx, ny, nn;
    unsigned char* data = stbi_load("Textures/earth.jpg", &nx, &ny, &nn, 0);
    std::shared_ptr<Texture> image = std::make_shared<ImageTexture>(data, nx, ny);

    list.push_back(std::make_shared<Sphere>(glm::vec3(0, -1000, 0), 1000.0, std::make_shared<Lambertian>(pertext)));
    list.push_back(std::make_shared<Sphere>(glm::vec3(0, 2, 0), 2, std::make_shared<Lambertian>(image)));

    auto lightTex = std::make_shared<ConstantTexture>(glm::vec3(4.0f));
    list.push_back(std::make_shared<XYRect>(3, 5, 1, 3, -2, std::make_shared<DiffuseLight>(lightTex)));
    //list.push_back(std::make_shared<Sphere>(glm::vec3(4, 2, -2), 1, std::make_shared<DiffuseLight>(lightTex)));
    return std::make_unique<HitableList>(list);
}

std::unique_ptr<Hitable> cornellBox(){
    std::vector<std::shared_ptr<Hitable>> list;

    auto red = std::make_shared<Lambertian>(std::make_shared<ConstantTexture>(glm::vec3(0.65,0.05,0.05)));
    auto white = std::make_shared<Lambertian>(std::make_shared<ConstantTexture>(glm::vec3(0.73)));
    auto green = std::make_shared<Lambertian>(std::make_shared<ConstantTexture>(glm::vec3(0.12,0.45,0.15)));
    auto light = std::make_shared<DiffuseLight>(std::make_shared<ConstantTexture>(glm::vec3(15)));

    list.push_back(std::make_shared<FlipNormals>(std::make_shared<YZRect>(0,555,0,555,555,green)));
    list.push_back(std::make_shared<YZRect>(0,555,0,555,0,red));
    list.push_back(std::make_shared<XZRect>(213,343,227,332,554,light));
    list.push_back(std::make_shared<FlipNormals>(std::make_shared<XZRect>(0,555,0,555,555,white)));
    list.push_back(std::make_shared<XZRect>(0,555,0,555,0,white));
    list.push_back(std::make_shared<FlipNormals>(std::make_shared<XYRect>(0,555,0,555,555,white)));
    return std::make_unique<HitableList>(list);
}

glm::vec3 color(const Ray& r, const Hitable& world, int depth){
    HitRecord rec;
    if(world.hit(r, 0.001, MAXFLOAT, rec)){
        Ray scattered;
        glm::vec3 attenuation;
        glm::vec3 emitted = rec.mat->emitted(rec.u, rec.v, rec.p);
        if(depth < 50 && rec.mat->scatter(r, rec, attenuation, scattered)){
            return emitted + attenuation * color(scattered, world, depth+1);
        } else {
            return emitted;
        }
    } else {
        /*glm::vec3 unit_direction = glm::normalize(r.direction);
        float t = 0.5f * (unit_direction.y + 1.0f);
        return (1.0f - t) * glm::vec3(1.0) + t * glm::vec3(0.5, 0.7, 1.0);
         */
        return glm::vec3(0.0f);
    }
}

int main() {
    int nx = 400;
    int ny = 300;
    int ns = 100;
    std::cout << "P3\n" << nx << " " << ny << "\n255\n";

    Perlin::init();

    //std::unique_ptr<Hitable> world = randomScene();
    //std::unique_ptr<Hitable> world = twoPerlinSpheres();
    std::unique_ptr<Hitable> world = cornellBox();

    glm::vec3 lookFrom(278,278,-800);
    glm::vec3 lookAt(278,278,0);
    float distToFocus = 10.0;
    float aperture = 0.0f;
    float vfov = 40.0f;
    Camera cam(lookFrom, lookAt, glm::vec3(0, 1, 0), vfov, float(nx) / float(ny), aperture, distToFocus);

    float gamma_correction = 1.0f / 2.2f;

    for(int j = ny-1; j >= 0; j--){
        for(int i = 0; i < nx; i++){
            glm::vec3 col(0.0);
            for(int s = 0; s < ns; s++){
                float u = (float(i) + randomFloat()) / float(nx);
                float v = (float(j) + randomFloat()) / float(ny);
                Ray r = cam.getRay(u, v);
                glm::vec3 tc = color(r, *world, 0);
                //tc.x = glm::min(tc.x, 1.0f);
                //tc.y = glm::min(tc.y, 1.0f);
                //tc.z = glm::min(tc.z, 1.0f);
                //col += tc;
                col += color(r, *world, 0);
            }
            col /= float(ns);
            col.x = glm::min(col.x, 1.0f);
            col.y = glm::min(col.y, 1.0f);
            col.z = glm::min(col.z, 1.0f);

            col = glm::vec3(glm::pow(col.r, gamma_correction), glm::pow(col.g, gamma_correction), glm::pow(col.b, gamma_correction));

            int ir = int(255.99 * col.r);
            int ig = int(255.99 * col.g);
            int ib = int(255.99 * col.b);
            std::cout << ir << " " << ig << " " << ib << std::endl;
        }
    }
}