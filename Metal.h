
#ifndef RAYTRACING_STUFF_METAL_H
#define RAYTRACING_STUFF_METAL_H

#include "Material.h"
#include "Randoms.h"

class Metal : public Material{
public:
    explicit Metal(std::shared_ptr<Texture> a, float f) : albedo(std::move(a)){
        if(f < 1){
            fuzz = f;
        } else{
            fuzz = 1;
        }
    }

    bool scatter(const Ray &r_in, const HitRecord &rec, glm::vec3& attenuation, Ray &scattered) const override {
        glm::vec3 reflected = glm::normalize(glm::reflect(r_in.direction, rec.normal));
        scattered = Ray(rec.p, reflected + fuzz * randomInUnitSphere());
        attenuation = albedo->value(rec.u,rec.v,rec.p);
        return (dot(scattered.direction, rec.normal) > 0);
    }

    std::shared_ptr<Texture> albedo;
    float fuzz;
};

#endif //RAYTRACING_STUFF_METAL_H
