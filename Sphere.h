
#ifndef RAYTRACING_STUFF_SPHERE_H
#define RAYTRACING_STUFF_SPHERE_H

#include <utility>

#include "Hitable.h"

class Sphere : public Hitable{
public:
    Sphere() = default;
    Sphere(glm::vec3 _center, float _r, std::shared_ptr<Material> m) : center(_center), radius(_r), mat(std::move(m)){}

    bool hit(const Ray &r, float t_min, float t_max, HitRecord &rec) const override {
        glm::vec3 oc = r.origin - center;
        float a = glm::dot(r.direction, r.direction);
        float b = glm::dot(oc, r.direction);
        float c = glm::dot(oc, oc) - radius * radius;
        float delta = b*b - a*c;
        if(delta > 0){
            float temp = (-b -glm::sqrt(delta)) / a;
            if(temp < t_max && temp > t_min){
                rec.t = temp;
                rec.p = r.pointAtParameter(rec.t);
                rec.normal = (rec.p - center) / radius;
                rec.mat = mat;
                getUV(rec.normal, rec.u, rec.v);
                return true;
            }
            temp = (-b +glm::sqrt(delta)) / a;
            if(temp < t_max && temp > t_min){
                rec.t = temp;
                rec.p = r.pointAtParameter(rec.t);
                rec.normal = (rec.p - center) / radius;
                rec.mat = mat;
                getUV(rec.normal, rec.u, rec.v);
                return true;
            }
        }
        return false;
    }

    bool boundingBox(AABB &box) const override {
        box = AABB(center - glm::vec3(radius), center + glm::vec3(radius));
        return true;
    }

    static void getUV(const glm::vec3& p, float&u, float& v){
        float phi = glm::atan(p.z, p.x);
        float theta = glm::asin(p.y);
        u = 1.0f - (phi + float(M_PI)) / (2.0f * float(M_PI));
        v = (theta + float(M_PI) / 2.0f) / float(M_PI);
    }

    glm::vec3 center;
    float radius;
    std::shared_ptr<Material> mat;
};

#endif //RAYTRACING_STUFF_SPHERE_H
