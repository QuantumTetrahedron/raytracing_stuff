cmake_minimum_required(VERSION 3.15)
project(raytracing_stuff)

set(CMAKE_CXX_STANDARD 17)

add_executable(raytracing_stuff main.cpp Ray.h Hitable.h Sphere.h HitableList.h Camera.h Material.h Lambertian.h Randoms.h Metal.h Dielectric.h AABB.h BVH.h Texture.h Perlin.h NoiseTexture.h ImageTexture.h stb_image.h DiffuseLight.h Rects.h FlipNormals.h)

file(COPY Textures DESTINATION .)