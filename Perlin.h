
#ifndef RAYTRACING_STUFF_PERLIN_H
#define RAYTRACING_STUFF_PERLIN_H

#include <glm/glm.hpp>
#include <vector>
#include <algorithm>
#include "Randoms.h"

class Perlin{
public:
    static void init(){
        ranvec = generate();
        perm_x = generatePerm();
        perm_y = generatePerm();
        perm_z = generatePerm();
    }

    static float noise(const glm::vec3& p) {
        int i = glm::floor(p.x);
        int j = glm::floor(p.y);
        int k = glm::floor(p.z);
        float u = p.x - glm::floor(p.x);
        float v = p.y - glm::floor(p.y);
        float w = p.z - glm::floor(p.z);
        std::array<std::array<std::array<glm::vec3, 2>, 2>, 2> c{};
        for(int di = 0; di < 2; di++){
            for(int dj = 0; dj < 2; dj++){
                for(int dk = 0; dk < 2; dk++){
                    c[di][dj][dk] = ranvec[perm_x[(i+di)&255]^perm_y[(j+dj)&255]^perm_z[(k+dk)&255]];
                }
            }
        }
        return trilinearInterp(c, u, v, w);
    }

    static float turb(const glm::vec3& p, int depth = 7) {
        float accum = 0;
        glm::vec3 temp_p = p;
        float weight = 1.0f;
        for(int i = 0; i < depth; i++){
            accum += weight * noise(temp_p);
            weight *= 0.5;
            temp_p *= 2;
        }
        return glm::abs(accum);
    }

private:

    static std::vector<glm::vec3> ranvec;
    static std::vector<int> perm_x, perm_y, perm_z;

    static std::vector<glm::vec3> generate(){
        std::vector<glm::vec3> p(256);
        std::generate(p.begin(), p.end(), [](){
            return glm::normalize(glm::vec3(-1+ 2 * randomFloat(), -1 + 2 * randomFloat(), -1 + 2 * randomFloat()));
        });
        return p;
    }

    static void permute(std::vector<int>& p){
        for(int i = p.size()-1; i > 0; i--){
            int target = int(randomFloat() * float(i + 1));
            std::swap(p[i], p[target]);
        }
    }

    static std::vector<int> generatePerm(){
        std::vector<int> p(256);
        std::iota(p.begin(), p.end(), 0);
        permute(p);
        return p;
    }

    static float trilinearInterp(const std::array<std::array<std::array<glm::vec3, 2>, 2>, 2>& c, float u, float v, float w){
        float uu = u*u*(3-2*u);
        float vv = v*v*(3-2*v);
        float ww = w*w*(3-2*w);
        float accum = 0;
        for(int i = 0; i < 2; i++){
            for(int j = 0; j < 2; j++){
                for(int k = 0; k<2; k++){
                    glm::vec3 weight_v(u-float(i), v-float(j), w-float(k));
                    accum += (float(i)*uu + (1.0f-float(i))*(1-uu)) *
                             (float(j)*vv + (1.0f-float(j))*(1-vv)) *
                             (float(k)*ww + (1.0f-float(k))*(1-ww)) *
                             glm::dot(c[i][j][k], weight_v);
                }
            }
        }
        return accum;
    }
};

std::vector<glm::vec3> Perlin::ranvec;
std::vector<int> Perlin::perm_x, Perlin::perm_y, Perlin::perm_z;

#endif //RAYTRACING_STUFF_PERLIN_H
