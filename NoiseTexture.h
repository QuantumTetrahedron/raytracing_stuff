
#ifndef RAYTRACING_STUFF_NOISETEXTURE_H
#define RAYTRACING_STUFF_NOISETEXTURE_H

#include "Texture.h"
#include "Perlin.h"

class NoiseTexture : public Texture{
public:
    NoiseTexture(float sc): scale(sc){}

    [[nodiscard]] glm::vec3 value(float u, float v, const glm::vec3 &p) const override {
        //return glm::vec3(1.0f) * 0.5f * (1 + Perlin::noise(scale * p));
        //return glm::vec3(1.0f) * Perlin::turb(scale * p);
        return glm::vec3(1.0f) * 0.5f * (1 + glm::sin(scale * p.z + 10 * Perlin::turb(p)));
    }

    float scale;
};

#endif //RAYTRACING_STUFF_NOISETEXTURE_H
