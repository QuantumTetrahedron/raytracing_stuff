
#ifndef RAYTRACING_STUFF_DIFFUSELIGHT_H
#define RAYTRACING_STUFF_DIFFUSELIGHT_H

#include "Material.h"
#include "Texture.h"

class DiffuseLight : public Material{
public:
    DiffuseLight(std::shared_ptr<Texture> a) : emit(std::move(a)){}

    bool scatter(const Ray &r_in, const HitRecord &rec, glm::vec3 &attenuation, Ray &scattered) const override {
        attenuation = emit->value(rec.u, rec.v, rec.p);
        return false;
    }

    [[nodiscard]] glm::vec3 emitted(float u, float v, const glm::vec3 &p) const override {
        return emit->value(u,v,p);
    }

    std::shared_ptr<Texture> emit;
};

#endif //RAYTRACING_STUFF_DIFFUSELIGHT_H
