
#ifndef RAYTRACING_STUFF_IMAGETEXTURE_H
#define RAYTRACING_STUFF_IMAGETEXTURE_H

#include "Texture.h"

class ImageTexture : public Texture{
public:
    ImageTexture(unsigned char* pixels, int A, int B) : data(pixels), nx(A), ny(B){}

    glm::vec3 value(float u, float v, const glm::vec3 &p) const override {
        int i = u*nx;
        int j = (1-v)*ny-0.001;
        if(i < 0) i = 0;
        if(j < 0) j = 0;
        if(i > nx-1) i = nx-1;
        if(j > ny-1) j = nx-1;
        float r = int(data[3*i+3*nx*j]) / 255.0f;
        float g = int(data[3*i+3*nx*j+1]) / 255.0f;
        float b = int(data[3*i+3*nx*j+2]) / 255.0f;
        return glm::vec3(r,g,b);
    }

    unsigned char* data;
    int nx, ny;
};

#endif //RAYTRACING_STUFF_IMAGETEXTURE_H
