
#ifndef RAYTRACING_STUFF_HITABLELIST_H
#define RAYTRACING_STUFF_HITABLELIST_H

#include <vector>
#include "Hitable.h"

class HitableList : public Hitable{
public:
    HitableList() = default;
    explicit HitableList(const std::vector<std::shared_ptr<Hitable>>& l){
        list = l;
    }

    bool hit(const Ray &r, float t_min, float t_max, HitRecord &rec) const override {
        HitRecord temp_rec;
        bool hit_anything = false;
        float closest = t_max;
        for(const auto& h : list){
            if(h->hit(r, t_min, closest, temp_rec)){
                hit_anything = true;
                closest = temp_rec.t;
                rec = temp_rec;
            }
        }
        return hit_anything;
    }

    bool boundingBox(AABB &box) const override {
        if(list.empty()) return false;
        AABB temp_box;
        bool first_true = list[0]->boundingBox(temp_box);
        if(!first_true) return false;
        else
            box = temp_box;

        for(int i = 1; i < list.size(); i++){
            if(list[i]->boundingBox(temp_box)){
                box = box.add(temp_box);
            } else {
                return false;
            }
        }
        return true;
    }

    std::vector<std::shared_ptr<Hitable>> list;
};

#endif //RAYTRACING_STUFF_HITABLELIST_H
