
#ifndef RAYTRACING_STUFF_RANDOMS_H
#define RAYTRACING_STUFF_RANDOMS_H

#include <random>
#include <glm/vec3.hpp>

float randomFloat(){
    static std::random_device rd;
    static std::mt19937 engine(rd());
    static std::uniform_real_distribution dist(0.0,1.0);
    return dist(engine);
}

glm::vec3 randomInUnitSphere(){
    glm::vec3 p;
    do{
        p = 2.0f * glm::vec3(randomFloat(), randomFloat(), randomFloat()) - glm::vec3(1.0);
    } while(p.x * p.x + p.y * p.y + p.z * p.z >= 1.0);
    return p;
}

glm::vec3 randomInUnitDisk(){
    glm::vec3 p;
    do{
        p = 2.0f * glm::vec3(randomFloat(), randomFloat(), 0) - glm::vec3(1.0f, 1.0f, 0.0f);
    }while(glm::dot(p,p) >= 1.0);
    return p;
}

#endif //RAYTRACING_STUFF_RANDOMS_H
