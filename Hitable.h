
#ifndef RAYTRACING_STUFF_HITABLE_H
#define RAYTRACING_STUFF_HITABLE_H

#include "Ray.h"
#include "AABB.h"
#include <memory>

class Material;

struct HitRecord{
    float t;
    glm::vec3 p;
    glm::vec3 normal;
    std::shared_ptr<Material> mat;
    float u, v;
};

class Hitable{
public:
    virtual bool hit(const Ray& r, float t_min, float t_max, HitRecord& rec) const = 0;
    virtual bool boundingBox(AABB& box) const = 0;
};

#endif //RAYTRACING_STUFF_HITABLE_H
